﻿# StoryCLMNavigation

## Подключение:
Скрипт должен быть подключен на каждом слайде

На слайде должны быть мета-теги:

```html
<meta name="clm-swipe-next" content="any-slide.html">
<meta name="clm-swipe-previous" content="any-slide.html">
```

Скрипт находится в файле js/storyclm-navigation-1.0.0.js

### Пример подключения:
```js
    window.storyCLMNavigation = new StoryCLMNavigation({
    	threshold: 200,
    	swipePointsCount: 1
    });

    window.storyCLMNavigation.onSwipePrev = function (direction) {
        window.location = direction;
    }


    window.storyCLMNavigation.onSwipeNext = function (direction) {
        window.location = direction;
    }
```

## Методы:
* **block** - блокирует/разблокирует навигацию и вперед и назад, принимает логический параметр (true/false)
* **blockNext** - блокирует/разблокирует навигацию вперед, принимает логический параметр (true/false)
* **blockPrev** - блокирует/разблокирует навигацию назад, принимает логический параметр (true/false)
* **blockSwipe** - единоразово блокируетстандартный хаммеровский свайп
* **isEmptyMeta** - проверяет мета-теги на пустоту


## Параметры:
* **treshold** - целое число, равное минимальному кол-ву пикселей, считающемуся за свайп, по умолчанию - 200
* **swipePointsCount** - кол-во пальцев, нужных для свайпа
* **onSwipeNext** - функция; событие которое произойдет после свайпа на следующий слайд
* **onSwipePrev** - функция; событие которое произойдет после свайпа на предыдущий слайд

## Зависимости:
* hammer.js

## Особенности:

Для **Communicate**:

* В код слайда нужно добавить блокировку стандартной навигации:
```js
CommunicateEmbedded.onslideflip = function (ev) {
    ev.preventDefault();
    return;
}
```

* Методы onSwipeNext, onSwipePrev будут выглядеть примерно так
```js
window.storyCLMNavigation.onSwipeNext = function (direction) {
	CommunicateEmbedded.navigate(direction, 'right');
}
```